import lightnet as ln
import torch

__all__ = ['params']


params = ln.engine.HyperParameters( 
    # Network
    class_label_map = ['person', 'bicycle', 'car', 'motorbike', 'aeroplane', 'bus', 'train', 'truck', 'boat', 'traffic light', 'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear', 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie', 'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove', 'skateboard', 'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple', 'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut', 'cake', 'chair', 'sofa', 'pottedplant', 'bed', 'diningtable', 'toilet', 'tvmonitor', 'laptop', 'mouse', 'remote', 'keyboard', 'cell phone', 'microwave', 'oven', 'toaster', 'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors', 'teddy bear', 'hair drier', 'toothbrush'],
    _input_dimension = (416, 416),
    _batch_size = 64,
    _mini_batch_size = 2,
    _max_batches = 1000,

    # # Dataset
    # _train_set = 'train.h5',
    # _test_set = 'test.h5',
    # _filter_anno = 'ignore',

    # Data Augmentation
    _jitter = .3,
    _flip = .5,
    _hue = .1,
    _saturation = 1.5,
    _value = 1.5,
)

# Network
def init_weights(m):
    if isinstance(m, torch.nn.Conv2d):
        torch.nn.init.kaiming_normal_(m.weight, nonlinearity='leaky_relu')

# params.network = ln.models.TinyYolo(len(params.class_label_map))
params.network = ln.models.TinyYoloV2(80, anchors=[(0.57273, 0.677385), (1.87446, 2.06253), (3.33843, 5.47434), (7.88282, 3.52778), (9.77052, 9.16828)])
# params.network = ln.models.TinyYoloV2(80, anchors=[(18.3274,21.6763), (59.9827,66.001), (106.83,175.179), (252.25,112.889), (312.657,293.385)])
# params.network.apply(init_weights)

# Loss
params.loss = ln.network.loss.RegionLoss(
    80,
    params.network.anchors,
    params.network.stride,
)

# Postprocessing
params._post = ln.data.transform.Compose([
    ln.data.transform.GetBoundingBoxes(len(params.class_label_map), params.network.anchors, 0.01),
    ln.data.transform.NonMaxSuppression(0.5),
    ln.data.transform.TensorToBrambox(params.input_dimension, params.class_label_map),
])

# Optimizer
params.optimizer = torch.optim.SGD(
    params.network.parameters(),
    lr = .0001 / (params.batch_size // params.mini_batch_size),
    momentum = .9,
    weight_decay = .0005 * (params.batch_size // params.mini_batch_size),
    dampening = 0,
)

# Scheduler
burn_in = torch.optim.lr_scheduler.LambdaLR(
    params.optimizer,
    lambda b: (b / 1000) ** 4,
)
step = torch.optim.lr_scheduler.MultiStepLR(
    params.optimizer,
    milestones = [400, 800],
    gamma = .1,
)
params.scheduler = ln.engine.SchedulerCompositor(
    (0,  step),
)
