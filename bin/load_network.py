# Basic imports
import lightnet as ln
import torch
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import brambox as bb

darknet_weights_path = '../weights/pretrained/yolov2-tiny.weights'
lightnet_models_path = '../models/tiny-yolo-pretrained_coco.pt'

# Settings
ln.logger.setConsoleLevel('DEBUG')             # Only show error log messages
bb.logger.setConsoleLevel('DEBUG')             # Only show error log messages

# Convert the weights to lightnet
darknet_model = ln.models.TinyYoloV2(num_classes=80, anchors=[(0.57273, 0.677385), (1.87446, 2.06253), (3.33843, 5.47434), (7.88282, 3.52778), (9.77052, 9.16828)])
darknet_model.load(darknet_weights_path)
print(darknet_model.layers)
# darknet_model.save(lightnet_models_path, remap=ln.models.TinyYoloV2.remap_darknet)
darknet_model.save(lightnet_models_path)

lightnet_model = ln.models.TinyYoloV2(num_classes=80, anchors=[(0.57273, 0.677385), (1.87446, 2.06253), (3.33843, 5.47434), (7.88282, 3.52778), (9.77052, 9.16828)])
lightnet_model.load(lightnet_models_path, strict=False)
print(lightnet_model.layers)
