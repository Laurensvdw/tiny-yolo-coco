# # Setup warnings
import warnings
from collections import OrderedDict
from math import isnan
from statistics import mean

import brambox as bb
import matplotlib.patches as patches
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch.nn as nn
import torch.nn.functional as F
import torch.quantization
import torchvision
from torch.quantization import QuantStub, DeQuantStub
from torch.utils.data import DataLoader
from tqdm import tqdm

import lightnet as ln
import lightnet.network as lnn
from coco_dataset import CocoDataset
from utils import *

warnings.filterwarnings(
    action='ignore',
    category=DeprecationWarning,
    module=r'.*'
)
warnings.filterwarnings(
    action='default',
    module=r'torch.quantization'
)

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')
# device = torch.device('cpu')

# Specify random seed for repeatable results
torch.manual_seed(191009)

class QuantizablePaddedMaxPool2d(nn.Module):
    """ Maxpool layer with a replicating padding.

    Args:
        kernel_size (int or tuple): Kernel size for maxpooling
        stride (int or tuple, optional): The stride of the window; Default ``kernel_size``
        padding (tuple, optional): (left, right, top, bottom) padding; Default **None**
        dilation (int or tuple, optional): A parameter that controls the stride of elements in the window
    """
    def __init__(self, kernel_size, stride=None, padding=(0, 0, 0, 0), dilation=1):
        super().__init__()
        self.kernel_size = kernel_size
        self.stride = stride or kernel_size
        self.padding = padding
        self.dilation = dilation

    def extra_repr(self):
        return f'kernel_size={self.kernel_size}, stride={self.stride}, padding={self.padding}, dilation={self.dilation}'

    def forward(self, x):
        m = nn.ReplicationPad2d(self.padding)
        if x.is_quantized:
            zero_point = x.q_zero_point()
            scale = x.q_scale()
            x = m(x.dequantize())
            x = torch.quantize_per_tensor(x, scale=scale, zero_point=zero_point, dtype=torch.quint8)
        else:
            x = m(x)
        x = F.max_pool2d(x, self.kernel_size, self.stride, 0, self.dilation)
        return x


class QuantizableTinyYoloV2(ln.models.TinyYoloV2):
    def __init__(self, num_classes=80, anchors=[(0.57273, 0.677385), (1.87446, 2.06253), (3.33843, 5.47434), (7.88282, 3.52778), (9.77052, 9.16828)],
                 input_channels=3):
        super().__init__(num_classes=num_classes, anchors=anchors, input_channels=input_channels)
        self.quant = QuantStub()
        self.dequant = DeQuantStub()

        self.layers = nn.Sequential(
            OrderedDict([
                ('1_convbatch', lnn.layer.Conv2dBatchReLU(input_channels, 16, 3, 1, 1)),
                ('2_max', nn.MaxPool2d(2, 2)),
                ('3_convbatch', lnn.layer.Conv2dBatchReLU(16, 32, 3, 1, 1)),
                ('4_max', nn.MaxPool2d(2, 2)),
                ('5_convbatch', lnn.layer.Conv2dBatchReLU(32, 64, 3, 1, 1)),
                ('6_max', nn.MaxPool2d(2, 2)),
                ('7_convbatch', lnn.layer.Conv2dBatchReLU(64, 128, 3, 1, 1)),
                ('8_max', nn.MaxPool2d(2, 2)),
                ('9_convbatch', lnn.layer.Conv2dBatchReLU(128, 256, 3, 1, 1)),
                ('10_max', nn.MaxPool2d(2, 2)),
                ('11_convbatch', lnn.layer.Conv2dBatchReLU(256, 512, 3, 1, 1)),
                ('12_max', lnn.layer.PaddedMaxPool2d(2, 1, (0, 1, 0, 1))),
                ('13_convbatch', lnn.layer.Conv2dBatchReLU(512, 1024, 3, 1, 1)),
                ('14_convbatch', lnn.layer.Conv2dBatchReLU(1024, 512, 3, 1, 1)),
                ('15_conv', nn.Conv2d(512, len(self.anchors) * (5 + self.num_classes), 1, 1, 0)),
            ])
        )

    def forward(self, x):
        x = self.quant(x)
        x =  self.layers(x)
        return self.dequant(x)


def imshow(img, labels, output):
    img = img      # unnormalize
    npimg = img.numpy()
    fig, ax = plt.subplots(1)
    ax.imshow(np.transpose(npimg, (1, 2, 0)))
    for i in range(len(labels)):
        rect = patches.Rectangle((labels['x_top_left'][i], labels['y_top_left'][i]), labels['width'][i], labels['height'][i], linewidth=1, edgecolor='r', facecolor='none')
        ax.add_patch(rect)
        plt.text(labels['x_top_left'][i], labels['y_top_left'][i], labels['class_label'][i])
    for i in range(len(output)):
        rect = patches.Rectangle((output['x_top_left'][i], output['y_top_left'][i]), output['width'][i], output['height'][i], linewidth=1, edgecolor='b', facecolor='none')
        ax.add_patch(rect)
        plt.text(output['x_top_left'][i], output['y_top_left'][i], output['class_label'][i])
    plt.show()


def evaluate(test_loader, network, loss, post, neval_batches, eval_device, show_images):
    params.to(eval_device)
    network.to(eval_device)
    network.eval()
    loss.eval()
    loss_dict = {'tot': [], 'coord': [], 'conf': [], 'cls': []}
    anno, det = [], []
    cnt = 0

    with torch.no_grad():
        for idx, (data, target) in enumerate(tqdm(test_loader, total=neval_batches)):
            cnt += 1
            data = data.to(eval_device)
            output = network(data)
            output = output.to(device)
            second_loss = loss(output, target)
            output = post(output)
            if show_images:
                imshow(torchvision.utils.make_grid(data.cpu()), target, output)

            num_img = data.shape[0]
            loss_dict['tot'].append(loss.loss_tot.item() * num_img)
            loss_dict['coord'].append(loss.loss_coord.item() * num_img)
            loss_dict['conf'].append(loss.loss_conf.item() * num_img)
            loss_dict['cls'].append(loss.loss_cls.item() * num_img)

            output.image = pd.Categorical.from_codes(output.image, dtype=target.image.dtype)
            anno.append(target)
            det.append(output)

            if cnt >= neval_batches:
                break

    anno = bb.util.concat(anno, ignore_index=True, sort=False)
    det = bb.util.concat(det, ignore_index=True, sort=False)

    aps = []
    for c in tqdm(params.class_label_map):
        anno_c = anno[anno.class_label == c]
        det_c = det[det.class_label == c]

        if len(det_c) == 0 or len(anno_c) == 0:
            continue

        # By default brambox considers ignored annos as regions -> we want to consider them as annos still
        matched_det = bb.stat.match_det(det_c, anno_c, 0.1, criteria=bb.stat.coordinates.iou,
                                        ignore=bb.stat.IgnoreMethod.SINGLE)
        pr = bb.stat.pr(matched_det, anno_c)

        ap = bb.stat.ap(pr)

        if not isnan(ap):
            aps.append(ap)

    print(aps)
    return round(100 * mean(aps), 2)


def load_model(model_file):
    model = QuantizableTinyYoloV2()
    model.load(model_file)
    model.to('cpu')
    return model


def print_size_of_model(model):
    torch.save(model.state_dict(), "temp.p")
    print('Size (MB):', os.path.getsize("temp.p") / 1e6)
    os.remove('temp.p')


def prepare_data_loaders(params):
    data_loader = torch.utils.data.DataLoader(
        CocoDataset('../data/coco/annotations/instances_train2014.json', params, False),
        batch_size=1,
        shuffle=True,
        drop_last=True,
        num_workers=1,
        pin_memory=True,
        collate_fn=ln.data.brambox_collate,
    )

    data_loader_test = torch.utils.data.DataLoader(
        CocoDataset('../data/coco/annotations/instances_val2014.json', params, False, 'val'),
        batch_size=1,
        shuffle=False,
        drop_last=False,
        num_workers=1,
        pin_memory=True,
        collate_fn=ln.data.brambox_collate,
    )

    return data_loader, data_loader_test


config_path = '/home/laurens/Desktop/masterproef/lightnet/tiny-yolo-coco/cfg/tinyyolo-coco.py'
saved_model_dir = '/home/laurens/Desktop/masterproef/lightnet/tiny-yolo-coco/models/'
float_model_file = 'tiny-yolo-pretrained_coco.pt'
weight_file = saved_model_dir + float_model_file
scripted_float_model_file = 'tinyYoloV2_quantization_scripted.pt'
scripted_quantized_model_file = 'tinyYoloV2_quantization_scripted_quantized.pt'

train_batch_size = 100
eval_batch_size = 100

params = ln.engine.HyperParameters.from_file(config_path)
if weight_file.endswith('.state.pt'):
    params.load(weight_file)
else:
    params.network.load(weight_file)

data_loader, data_loader_test = prepare_data_loaders(params)
float_model = params.network

float_model = load_model(saved_model_dir + float_model_file)

print(float_model)

float_model.eval()

num_eval_batches = 100

print("Size of baseline model")
print_size_of_model(float_model)

mAP = evaluate(data_loader_test, float_model, params.loss, params.post, num_eval_batches, device, False)
print('\nMean Average Precision (mAP): %.3f' % mAP)


num_calibration_batches = 100

static_quantized_model = load_model(saved_model_dir + float_model_file)
static_quantized_model.eval()

# Specify quantization configuration
# Start with simple min/max range estimation and per-tensor quantization of weights
static_quantized_model.qconfig = torch.quantization.default_qconfig
print(static_quantized_model.qconfig)
torch.quantization.prepare(static_quantized_model, inplace=True)

# Calibrate with the training set
evaluate(data_loader_test, static_quantized_model, params.loss, params.post, num_eval_batches, device, False)
print('Post Training Quantization: Calibration done')

# Convert to quantized model
static_quantized_model.to('cpu')
static_quantized_model.eval()
torch.quantization.convert(static_quantized_model, inplace=True)
print('Post Training Quantization: Convert done')

print(static_quantized_model)

print("Size of model after static quantization")
print_size_of_model(static_quantized_model)

mAP = evaluate(data_loader_test, static_quantized_model, params.loss, params.post, num_eval_batches, torch.device('cpu'), False)
print('\nMean Average Precision (mAP): %.3f' % mAP)

# dynamic_quantized_model = torch.quantization.quantize_dynamic(
#     float_model, {torch.nn.Linear, lnn.layer.Conv2dBatchReLU}, dtype=torch.qint8
# )
#
# print("Size of model after dynamic quantization")
# print_size_of_model(dynamic_quantized_model)
