import brambox as bb
from PIL import ImageFile
from PIL import Image
from torchvision import transforms as tf

import lightnet as ln
import lightnet.data as lnd

ImageFile.LOAD_TRUNCATED_IMAGES = True

def identify_train_file(img_id):
    return f'../data/coco/images/train2014/{img_id}.jpg'

def identify_val_file(img_id):
    return f'../data/coco/images/val2014/{img_id}.jpg'

class CocoDataset(ln.models.BramboxDataset):
    def __init__(self, anno_file, params, augment, type='train'):
        annos = bb.io.load(bb.io.parser.annotation.CocoParser, anno_file)

        lb = ln.data.transform.Letterbox(dataset=self)
        img_tf = ln.data.transform.Compose([lb, tf.ToTensor()])
        anno_tf = ln.data.transform.Compose([lb])
        if augment:
            rf = ln.data.transform.RandomFlip(params.flip)
            rc = ln.data.transform.RandomJitter(params.jitter, True, 0.1)
            hsv = ln.data.transform.RandomHSV(params.hue, params.saturation, params.value)
            img_tf[0:0] = [hsv, rc, rf]
            anno_tf[0:0] = [rc, rf]

        if type == 'val':
            super().__init__(annotations=annos, input_dimension=params.input_dimension,
                             class_label_map=params.class_label_map, identify=identify_val_file, img_transform=img_tf,
                             anno_transform=anno_tf)
        else:
            super().__init__(annotations=annos, input_dimension=params.input_dimension, class_label_map=params.class_label_map, identify=identify_train_file, img_transform=img_tf, anno_transform=anno_tf)

    @lnd.Dataset.resize_getitem
    def __getitem__(self, index):
        """ Get transformed image and annotations based of the index of ``self.keys``

                Args:
                    index (int): index of the ``self.keys`` list containing all the image identifiers of the dataset.

                Returns:
                    tuple: (transformed image, list of transformed brambox boxes)
                """
        if index >= len(self):
            raise IndexError(f'list index out of range [{index}/{len(self) - 1}]')

        # Load
        # print(f'opening image: {self.id(self.keys[index])}')
        img = Image.open(self.id(self.keys[index]))
        img = img.convert('RGB')
        anno = bb.util.select_images(self.annos, [self.keys[index]])

        # Transform
        if self.img_tf is not None:
            img = self.img_tf(img)
        if self.anno_tf is not None:
            anno = self.anno_tf(anno)

        return img, anno
