import argparse

from distiller.quantization import LinearQuantMode
from distiller.quantization.range_linear import QuantAwareTrainRangeLinearQuantizer
import torch
import lightnet as ln

def quantize_model_for_training(args, params):

    quantizer = QuantAwareTrainRangeLinearQuantizer(model=params.network, optimizer=params.optimizer,
                 bits_activations=args.bits_activations, bits_weights=args.bits_weights, bits_bias=args.bits_bias,
                mode=LinearQuantMode.ASYMMETRIC_UNSIGNED)

    dummy_input = torch.randn(1, 3, 416, 416)

    quantizer.prepare_model(dummy_input)
    quantizer.quantize_params()

    return quantizer.model

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Quantize and train network',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('weight', help='Path to weight file', default=None, nargs='?')
    parser.add_argument('-n', '--network', help='network config file', required=True)
    parser.add_argument('-c', '--cuda', action='store_true', help='Use cuda')
    parser.add_argument('-b', '--backup', metavar='folder', help='Backup folder', default='./backup')
    parser.add_argument('-v', '--visdom', action='store_true', help='Visualize training data with visdom')
    parser.add_argument('-e', '--visdom_env', help='Visdom environment to plot to', default='main')
    parser.add_argument('-p', '--visdom_port', help='Port of the visdom server', type=int, default=8080)
    parser.add_argument('-r', '--visdom_rate', help='How often to plot to visdom (batches)', type=int, default=1)
    parser.add_argument('-a', '--anno', help='annotation folder', default='../data/coco/annotations/instances_train2014.json')
    parser.add_argument('-bitsa', '--bits_activations', type=int, default=8)
    parser.add_argument('-bitsw', '--bits_weights', type=int, default=8)
    parser.add_argument('-bitsb', '--bits_bias', type=int, default=32)
    args = parser.parse_args()

    # Parse arguments
    device = torch.device('cpu')

    params = ln.engine.HyperParameters.from_file(args.network)
    if args.weight is not None:
        if args.weight.endswith('.state.pt'):
            params.load(args.weight)
        else:
            params.network.load(args.weight, strict=False)  # Disable strict mode for loading partial weights

    print(params.network)
    params.network = quantize_model_for_training(args, params)
    print(params.network)
    print(params.network.layers[0].layers)