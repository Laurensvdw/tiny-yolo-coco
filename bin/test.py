#!/usr/bin/env python
import argparse
import logging
from math import isnan
from statistics import mean
import sys
sys.path.append('/home/laurens/Desktop/masterproef/distiller')
sys.path.append('/home/laurens/Desktop/masterproef/lightnet')

import brambox as bb
import matplotlib.patches as patches
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
import torchvision
from PIL import Image
from tqdm import tqdm

import distiller
import lightnet as ln
from coco_dataset import CocoDataset

log = logging.getLogger('lightnet.TinyYolo.COCO.test')

def imshow(img, labels, output):
    img = img      # unnormalize
    npimg = img.numpy()
    fig, ax = plt.subplots(1)
    ax.imshow(np.transpose(npimg, (1, 2, 0)))
    for i in range(len(labels)):
        rect = patches.Rectangle((labels['x_top_left'][i], labels['y_top_left'][i]), labels['width'][i], labels['height'][i], linewidth=1, edgecolor='r', facecolor='none')
        ax.add_patch(rect)
        plt.text(labels['x_top_left'][i], labels['y_top_left'][i], labels['class_label'][i])
    for i in range(len(output)):
        rect = patches.Rectangle((output['x_top_left'][i], output['y_top_left'][i]), output['width'][i], output['height'][i], linewidth=1, edgecolor='b', facecolor='none')
        ax.add_patch(rect)
        plt.text(output['x_top_left'][i], output['y_top_left'][i], output['class_label'][i])
    plt.show()

class TestEngine:
    def __init__(self, params, dataloader, **kwargs):
        self.params = params
        self.dataloader = dataloader

        # extract data from params
        self.post = params.post
        self.loss = params.loss
        self.network = params.network

        # Setting kwargs
        for k, v in kwargs.items():
            if not hasattr(self, k):
                setattr(self, k, v)
            else:
                log.error('{k} attribute already exists on TestEngine, not overwriting with `{v}`')

    def __call__(self):
        self.params.to(self.device)
        self.network.eval()
        self.loss.eval()    # This is necessary so the loss doesnt use its 'prefill' rule

        if self.loss_format == 'none':
            anno, det = self.test_none()
        else:
            anno, det = self.test_loss()

        aps = []
        aps1 = []
        aps2 = []
        for c in tqdm(self.params.class_label_map):
            anno_c = anno[anno.class_label == c]
            det_c = det[det.class_label == c]

            if len(det_c) == 0 and len(anno_c) == 0:
                continue

            if len(det_c) == 0 or len(anno_c) == 0:
                aps.append(0.0)
                aps1.append(0.0)
                aps2.append(0.0)
                continue

            # By default brambox considers ignored annos as regions -> we want to consider them as annos still
            matched_det = bb.stat.match_det(det_c, anno_c, 0.1, criteria=bb.stat.coordinates.iou, ignore=bb.stat.IgnoreMethod.SINGLE)
            matched_det1 = bb.stat.match_det(det_c, anno_c, 0.25, criteria=bb.stat.coordinates.iou, ignore=bb.stat.IgnoreMethod.SINGLE)
            matched_det2 = bb.stat.match_det(det_c, anno_c, 0.5, criteria=bb.stat.coordinates.iou, ignore=bb.stat.IgnoreMethod.SINGLE)
            pr = bb.stat.pr(matched_det, anno_c)
            pr1 = bb.stat.pr(matched_det1, anno_c)
            pr2 = bb.stat.pr(matched_det2, anno_c)
            ap = bb.stat.ap(pr)
            ap1 = bb.stat.ap(pr1)
            ap2 = bb.stat.ap(pr2)

            if not isnan(ap):
                aps.append(ap)

            if not isnan(ap2):
                aps2.append(ap2)

            if not isnan(ap1):torch

                aps1.append(ap1)

        # print(aps)
        # print(aps1)
        # print(aps2)
        m_ap = round(100 * mean(aps), 2)
        m_ap1 = round(100 * mean(aps1), 2)
        m_ap2 = round(100 * mean(aps2), 2)
        print(f'mAP: {m_ap:.2f}%')
        print(f'mAP: {m_ap1:.2f}%')
        print(f'mAP: {m_ap2:.2f}%')

        if self.detection is not None:
            def get_img_dim(name):
                with Image.open(f'../data/coco/images/train2014/{name}.jpg') as img:
                    return img.size

            rlb = ln.data.transform.ReverseLetterbox(self.params.input_dimension, get_img_dim)
            det = rlb(det)
            bb.io.save(det, 'pandas', self.detection)

    def test_none(self):
        anno, det = [], []

        with torch.no_grad():
            for idx, (data, target) in enumerate(tqdm(self.dataloader)):
                data = data.to(self.device)
                output = self.network(data)
                output = self.post(output)

                output.image = pd.Categorical.from_codes(output.image, dtype=target.image.dtype)
                anno.append(target)
                det.append(output)

        anno = bb.util.concat(anno, ignore_index=True, sort=False)
        det = bb.util.concat(det, ignore_index=True, sort=False)
        return anno, det

    def test_loss(self):
        loss_dict = {'tot': [], 'coord': [], 'conf': [], 'cls': []}
        anno, det = [], []

        with torch.no_grad():
            for idx, (data, target) in enumerate(tqdm(self.dataloader)):
                data = data.to(self.device)
                output = self.network(data)
                loss = self.loss(output, target)
                output = self.post(output)  # more options can be specified also
                # print('\n' + output.to_string())
                # imshow(torchvision.utils.make_grid(data.cpu()), target, output)

                num_img = data.shape[0]
                loss_dict['tot'].append(self.loss.loss_tot.item() * num_img)
                loss_dict['coord'].append(self.loss.loss_coord.item() * num_img)
                loss_dict['conf'].append(self.loss.loss_conf.item() * num_img)
                loss_dict['cls'].append(self.loss.loss_cls.item() * num_img)

                output.image = pd.Categorical.from_codes(output.image, dtype=target.image.dtype)
                anno.append(target)
                det.append(output)

        anno = bb.util.concat(anno, ignore_index=True, sort=False)
        det = bb.util.concat(det, ignore_index=True, sort=False)

        loss_tot = sum(loss_dict['tot']) / len(anno.image.cat.categories)
        loss_coord = sum(loss_dict['coord']) / len(anno.image.cat.categories)
        loss_conf = sum(loss_dict['conf']) / len(anno.image.cat.categories)
        loss_cls = sum(loss_dict['cls']) / len(anno.image.cat.categories)
        if self.loss == 'percent':
            loss_coord *= 100 / loss_tot
            loss_conf *= 100 / loss_tot
            loss_cls *= 100 / loss_tot
            log.info(f'Loss:{loss_tot:.5f} (Coord:{loss_coord:.2f}% Conf:{loss_conf:.2f}% Class:{loss_cls:.2f}%)')
        else:
            log.info(f'Loss:{loss_tot:.5f} (Coord:{loss_coord:.2f} Conf:{loss_conf:.2f} Class:{loss_cls:.2f})')

        return anno, det


class ListToTensor(object):
    def __call__(self, list):
        return torch.FloatTensor(list)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Test trained network',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('weight', help='Path to weight file', default='../models/tiny-yolo-pretrained_coco.pt')
    parser.add_argument('-n', '--network', help='network config file', default='../cfg/tinyyolo-coco.py')
    parser.add_argument('-c', '--cuda', action='store_true', help='Use cuda')
    parser.add_argument('-t', '--thresh', help='Detection Threshold', type=float, default=0.01)
    parser.add_argument('-l', '--loss', help='How to display loss', choices=['abs', 'percent', 'none'], default='abs')
    parser.add_argument('-a', '--anno', help='annotation folder', default='../data/coco/annotations/instances_train2014.json')
    parser.add_argument('-d', '--det', help='Detection pandas file', default=None)
    parser.add_argument('-q', '--quantized', action='store_true', help='Is a quantized network')
    parser.add_argument('-bitsa', '--bits_activations', type=int, default=8)
    parser.add_argument('-bitsw', '--bits_weights', type=int, default=8)
    parser.add_argument('-bitsb', '--bits_bias', type=int, default=32)
    parser.add_argument('-comp', '--compress', help='compression file', default='../cfg/quant_aware_train_linear_quant.yaml')
    args = parser.parse_args()

    # Parse arguments
    device = torch.device('cpu')
    if args.cuda:
        if torch.cuda.is_available():
            log.debug('CUDA enabled')
            device = torch.device('cuda')
        else:
            log.error('CUDA not available')

    model = torch.load(args.weight)

    params = ln.engine.HyperParameters.from_file(args.network)
    if args.quantized:
        # params.network = quantize_model_for_training(args, params)
        compression_scheduler = None
        compression_scheduler = distiller.file_config(params.network, params.optimizer, args.compress,
                                                      compression_scheduler)
    if args.weight.endswith('.state.pt'):
        params.load(args.weight)
    else:
        params.network.load(args.weight)

    if args.thresh is not None: # Overwrite threshold
        params.post[0].conf_thresh = args.thresh

    # # Dataloader
    # testing_dataloader = torch.utils.data.DataLoader(
    #     CocoDataset(args.anno, params, False),
    #     batch_size = 1,
    #     shuffle = False,
    #     drop_last = False,
    #     num_workers = 8,
    #     pin_memory = True,
    #     collate_fn = ln.data.brambox_collate,
    # )

    testing_dataloader = torch.utils.data.DataLoader(
        CocoDataset('../data/coco/annotations/instances_val2014.json', params, False, 'val'),
        batch_size=1,
        shuffle=False,
        drop_last=False,
        num_workers=1,
        pin_memory=True,
        collate_fn=ln.data.brambox_collate,
    )

    # Start test
    eng = TestEngine(
        params, testing_dataloader,
        device=device,
        loss_format=args.loss,
        detection=args.det,
    )
    eng()
