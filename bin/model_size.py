import os

import torch

import lightnet as ln

def print_size_of_model(model):
    torch.save(model.state_dict(), "temp.p")
    print('Size (MB):', os.path.getsize("temp.p") / 1e6)
    os.remove('temp.p')

normal_model = ln.models.TinyYoloV2(num_classes=80, anchors=[(0.57273, 0.677385), (1.87446, 2.06253), (3.33843, 5.47434), (7.88282, 3.52778), (9.77052, 9.16828)])
normal_model.load('/home/laurens/Desktop/masterproef/lightnet/tiny-yolo-coco/models/tiny-yolo-pretrained_coco.pt', strict=False)
print_size_of_model(normal_model)

quant_model = ln.models.TinyYoloV2(num_classes=80, anchors=[(0.57273, 0.677385), (1.87446, 2.06253), (3.33843, 5.47434), (7.88282, 3.52778), (9.77052, 9.16828)])
quant_model.load('/home/laurens/Desktop/masterproef/lightnet/tiny-yolo-coco/models/_untrained_checkpoint.pth.tar', strict=False)
print_size_of_model(quant_model)