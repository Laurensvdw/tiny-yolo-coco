python train_quantized.py -c -n ../cfg/tinyyolo-coco-quantized.py -comp ../cfg/quant_aware_train_linear_quant_8bitw_8bita_pc.yaml ../models/tiny-yolo-pretrained_coco.pt
mv ./backup/final.pt ../weights/trained/asymmetric_unsigned/tiny_yolo_coco_8bitw_8bita_pc.pt
mv ./backup/weights_500.pt ../weights/trained/asymmetric_unsigned/tiny_yolo_coco_8bitw_8bita_pc_weights_500.pt

python train_quantized.py -c -n ../cfg/tinyyolo-coco-quantized.py -comp ../cfg/quant_aware_train_linear_quant_8bitw_8bita_pl.yaml ../models/tiny-yolo-pretrained_coco.pt
mv ./backup/final.pt ../weights/trained/asymmetric_unsigned/tiny_yolo_coco_8bitw_8bita_pl.pt
mv ./backup/weights_500.pt ../weights/trained/asymmetric_unsigned/tiny_yolo_coco_8bitw_8bita_pl_weights_500.pt

python train_quantized.py -c -n ../cfg/tinyyolo-coco-quantized.py -comp ../cfg/quant_aware_train_linear_quant_4bitw_8bita_pc.yaml ../models/tiny-yolo-pretrained_coco.pt
mv ./backup/final.pt ../weights/trained/asymmetric_unsigned/tiny_yolo_coco_4bitw_8bita_pc.pt
mv ./backup/weights_500.pt ../weights/trained/asymmetric_unsigned/tiny_yolo_coco_4bitw_8bita_pc_weights_500.pt

python train_quantized.py -c -n ../cfg/tinyyolo-coco-quantized.py -comp ../cfg/quant_aware_train_linear_quant_4bitw_8bita_pl.yaml ../models/tiny-yolo-pretrained_coco.pt
mv ./backup/final.pt ../weights/trained/asymmetric_unsigned/tiny_yolo_coco_4bitw_8bita_pl.pt
mv ./backup/weights_500.pt ../weights/trained/asymmetric_unsigned/tiny_yolo_coco_4bitw_8bita_pl_weights_500.pt

python train_quantized.py -c -n ../cfg/tinyyolo-coco-quantized.py -comp ../cfg/quant_aware_train_linear_quant_4bitw_4bita_pc.yaml ../models/tiny-yolo-pretrained_coco.pt
mv ./backup/final.pt ../weights/trained/asymmetric_unsigned/tiny_yolo_coco_4bitw_4bita_pc.pt
mv ./backup/weights_500.pt ../weights/trained/asymmetric_unsigned/tiny_yolo_coco_4bitw_4bita_pc_weights_500.pt

python train_quantized.py -c -n ../cfg/tinyyolo-coco-quantized.py -comp ../cfg/quant_aware_train_linear_quant_4bitw_4bita_pl.yaml ../models/tiny-yolo-pretrained_coco.pt
mv ./backup/final.pt ../weights/trained/asymmetric_unsigned/tiny_yolo_coco_4bitw_4bita_pl.pt
mv ./backup/weights_500.pt ../weights/trained/asymmetric_unsigned/tiny_yolo_coco_4bitw_4bita_pl_weights_500.pt