import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import torch
import torchvision
import pandas as pd

import lightnet as ln
from coco_dataset import CocoDataset

params = ln.engine.HyperParameters.from_file('cfg/tinyyolo-coco.py')

testing_dataloader = torch.utils.data.DataLoader(
        CocoDataset('data/coco/annotations/instances_val2014.json', params, False, 'val'),
        batch_size = 1,
        shuffle = False,
        drop_last = False,
        num_workers = 1,
        pin_memory = True,
        collate_fn = ln.data.brambox_collate,
    )

training_loader = ln.data.DataLoader(
        CocoDataset('data/coco/annotations/instances_train2014.json', params, False),
        batch_size=params.mini_batch_size,
        shuffle=False,
        drop_last=True,
        num_workers=8,
        pin_memory=True,
        collate_fn=ln.data.brambox_collate,
    )

classes = ('person', 'bicycle', 'car', 'motorbike', 'aeroplane', 'bus', 'train', 'truck', 'boat', 'traffic light', 'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear', 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie', 'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove', 'skateboard', 'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple', 'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut', 'cake', 'chair', 'sofa', 'pottedplant', 'bed', 'diningtable', 'toilet', 'tvmonitor', 'laptop', 'mouse', 'remote', 'keyboard', 'cell phone', 'microwave', 'oven', 'toaster', 'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors', 'teddy bear', 'hair drier', 'toothbrush')

# functions to show an image
def imshow(img, labels):
    img = img      # unnormalize
    npimg = img.numpy()
    fig, ax = plt.subplots(1)
    ax.imshow(np.transpose(npimg, (1, 2, 0)))
    for i in range(len(labels)):
        rect = patches.Rectangle((labels['x_top_left'][i], labels['y_top_left'][i]), labels['width'][i], labels['height'][i], linewidth=1, edgecolor='r', facecolor='none')
        ax.add_patch(rect)
        plt.text(labels['x_top_left'][i], labels['y_top_left'][i], labels['class_label'][i])
    plt.show()

# get some random training images
dataiter = iter(training_loader)
images, labels = dataiter.__next__()
imshow(torchvision.utils.make_grid(images), labels)
images, labels = dataiter.__next__()
imshow(torchvision.utils.make_grid(images), labels)
images, labels = dataiter.__next__()
imshow(torchvision.utils.make_grid(images), labels)
images, labels = dataiter.__next__()
imshow(torchvision.utils.make_grid(images), labels)

