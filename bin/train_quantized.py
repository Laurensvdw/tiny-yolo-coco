#!/usr/bin/env python
import argparse
import logging
import os
import sys
sys.path.append('/home/laurens/Desktop/masterproef/distiller')
sys.path.append('/home/laurens/Desktop/masterproef/lightnet')
import time
from math import isinf, isnan
from statistics import mean

import numpy as np
import torch
import visdom

import distiller
import lightnet as ln
from coco_dataset import CocoDataset
from quantize import quantize_model_for_training


log = logging.getLogger('lightnet.COCO.train.quantized')
log.setLevel(logging.DEBUG)


class TrainEngine(ln.engine.Engine):
    def start(self):
        self.params.to(self.device)
        self.dataloader.change_input_dim()
        self.optimizer.zero_grad()

        self.train_loss = {'tot': [], 'coord': [], 'conf': [], 'cls': []}
        self.plot_train_loss = ln.engine.LinePlotter(self.visdom, 'train_loss',
                                                     opts=dict(xlabel='Batch', ylabel='Loss', title='Training Loss',
                                                               showlegend=True, legend=['Total loss', 'Coordinate loss',
                                                                                        'Confidence loss',
                                                                                        'Class loss']))
        self.plot_lr = ln.engine.LinePlotter(self.visdom, 'learning_rate', name='Learning Rate',
                                             opts=dict(xlabel='Batch', ylabel='Learning Rate',
                                                       title='Learning Rate Schedule'))
        self.batch_end(self.plot_rate)(self.plot)

        # self.best = 5.0

    def process_batch(self, data):
        data, target = data
        data = data.to(self.device)

        out = self.network(data)
        loss = self.loss(out, target) / self.batch_subdivisions
        loss.backward()

        self.train_loss['tot'].append(self.loss.loss_tot.item())
        self.train_loss['coord'].append(self.loss.loss_coord.item())
        self.train_loss['conf'].append(self.loss.loss_conf.item())
        self.train_loss['cls'].append(self.loss.loss_cls.item())

    def train_batch(self):
        # print([x.device for x in list(self.params.network.parameters())])
        self.optimizer.step()
        self.optimizer.zero_grad()
        self.scheduler.step(self.batch, epoch=self.batch)

        # By this call, distiller will update the quantized weights
        self.compression_scheduler.on_minibatch_end(self.epoch, None, None)

        # Get values from last batch
        tot = mean(self.train_loss['tot'][-self.batch_subdivisions:])
        coord = mean(self.train_loss['coord'][-self.batch_subdivisions:])
        conf = mean(self.train_loss['conf'][-self.batch_subdivisions:])
        cls = mean(self.train_loss['cls'][-self.batch_subdivisions:])
        self.log(f'{self.batch} Loss:{tot:.5f} (Coord:{coord:.2f} Conf:{conf:.2f} Cls:{cls:.2f})')

        if isinf(tot) or isnan(tot):
            log.error('Infinite loss')
            self.sigint = True
            return

        # if tot <= self.best:
        #     self.params.network.save(os.path.join(self.backup_folder, f'weights_{tot}.pt'))
        #     log.info(f'Saved better backup: {tot}')
        #     self.best = tot

    def plot(self):
        tot = mean(self.train_loss['tot'])
        coord = mean(self.train_loss['coord'])
        conf = mean(self.train_loss['conf'])
        cls = mean(self.train_loss['cls'])
        self.train_loss = {'tot': [], 'coord': [], 'conf': [], 'cls': []}

        self.plot_train_loss(np.array([[tot, coord, conf, cls]]), np.array([self.batch]))
        self.plot_lr(np.array([self.optimizer.param_groups[0]['lr']]), np.array([self.batch]))


    @ln.engine.Engine.epoch_start()
    def compression_at_start(self):
        self.compression_scheduler.on_epoch_begin(self.epoch)


    @ln.engine.Engine.epoch_end()
    def compression_at_end(self):
        self.compression_scheduler.on_epoch_end(self.epoch, self.optimizer)


    @ln.engine.Engine.batch_end(500)
    def backup(self):
        self.params.network.save(os.path.join(self.backup_folder, f'weights_{self.batch}.pt'))
        log.info(f'Saved backup')

    @ln.engine.Engine.batch_end(10)
    def resize(self):
        if self.batch >= self.max_batches - 200:
            self.dataloader.change_input_dim(self.input_dimension, None)
        else:
            self.dataloader.change_input_dim()

    def quit(self):
        if self.batch >= self.max_batches:
            self.params.network.save(os.path.join(self.backup_folder, 'final.pt'))
            return True
        elif self.sigint:
            self.params.save(os.path.join(self.backup_folder, 'backup.state.pt'))
            return True
        else:
            return False


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Quantize and train network',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('weight', help='Path to weight file', default=None, nargs='?')
    parser.add_argument('-n', '--network', help='network config file', required=True)
    parser.add_argument('-c', '--cuda', action='store_true', help='Use cuda')
    parser.add_argument('-b', '--backup', metavar='folder', help='Backup folder', default='./backup')
    parser.add_argument('-v', '--visdom', action='store_true', help='Visualize training data with visdom')
    parser.add_argument('-e', '--visdom_env', help='Visdom environment to plot to', default='main')
    parser.add_argument('-p', '--visdom_port', help='Port of the visdom server', type=int, default=8080)
    parser.add_argument('-r', '--visdom_rate', help='How often to plot to visdom (batches)', type=int, default=1)
    parser.add_argument('-a', '--anno', help='annotation folder', default='../data/coco/annotations/instances_train2014.json')
    parser.add_argument('-comp', '--compress', help='compression file', default='../cfg/quant_aware_train_linear_quant.yaml')
    parser.add_argument('-q', '--quantized', action='store_true', help='Pretrained is quantized')
    args = parser.parse_args()

    # Parse arguments
    device = torch.device('cpu')
    if args.cuda:
        if torch.cuda.is_available():
            log.info('CUDA enabled')
            device = torch.device('cuda')
        else:
            log.error('CUDA not available')

    if not os.path.isdir(args.backup):
        if not os.path.exists(args.backup):
            log.warning('Backup folder does not exist, creating...')
            os.makedirs(args.backup)
        else:
            raise ValueError('Backup path is not a folder')

    if args.visdom:
        visdom = visdom.Visdom(port=args.visdom_port, env=args.visdom_env)
    else:
        visdom = None

    params = ln.engine.HyperParameters.from_file(args.network)

    # Needed for distiller to work
    params.network.to(device)

    if args.quantized:
        compression_scheduler = None
        compression_scheduler = distiller.file_config(params.network, params.optimizer, args.compress,
                                                      compression_scheduler)
    if args.weight is not None:
        if args.weight.endswith('.state.pt'):
            params.load(args.weight)
        else:
            params.network.load(args.weight, strict=False)  # Disable strict mode for loading partial weights

    print(params.network)
    # params.network = quantize_model_for_training(args, params)
    # print(params.network)

    if not args.quantized:
        compression_scheduler = None
        compression_scheduler = distiller.file_config(params.network, params.optimizer, args.compress, compression_scheduler)

    #print(params.network)

    # Dataloader
    training_loader = ln.data.DataLoader(
        CocoDataset(args.anno, params, False),
        batch_size=params.mini_batch_size,
        shuffle=False,
        drop_last=True,
        num_workers=8,
        pin_memory=True,
        collate_fn=ln.data.brambox_collate,
    )

    # Start training
    # eng = TrainEngine(
    #     params, training_loader,
    #     device=device, visdom=visdom, plot_rate=args.visdom_rate, backup_folder=args.backup
    # )
    eng = TrainEngine(
        params, training_loader,
        device=device, visdom=visdom, plot_rate=args.visdom_rate, backup_folder=args.backup, compression_scheduler=compression_scheduler
    )
    b1 = eng.batch
    t1 = time.time()
    eng()
    t2 = time.time()
    b2 = eng.batch
    log.info(f'Training {b2 - b1} batches took {t2 - t1:.2f} seconds [{(t2 - t1) / (b2 - b1):.3f} sec/batch')
